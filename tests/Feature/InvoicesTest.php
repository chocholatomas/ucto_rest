<?php

namespace Tests\Feature;

use App\User;
use App\Invoice;
use App\Customer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InvoicesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_store_invoice()
    {
        $customer = factory(Customer::class)->state('withUser')->create();

        $invoice = factory(Invoice::class)->make();
        
        $response = $this->actingAs($customer->user, 'api')->postJson(route('customers.invoices.store', $customer->id), $invoice->attributesToArray());

        $response->assertStatus(201)
                 ->assertJsonStructure([
                     'data' => [
                         'name',
                         'number',
                         'published_at',
                         'expires_at',
                         'payed_at',
                         'payment_type'
                     ]
                 ])
                 ->assertJson([
                     'data' => $invoice->attributesToArray()
                 ]);

        $this->assertDatabaseHas('invoices', $invoice->attributesToArray());
    }

    public function test_user_can_update_invoice()
    {
        $invoice = factory(Invoice::class)->state('withCustomer')->create();
        $newInvoice = factory(Invoice::class)->make();

        $response = $this->actingAs($invoice->customer->user, 'api')->putJson(route('invoices.update', $invoice->id), $newInvoice->attributesToArray());

        $response->assertStatus(200)
                 ->assertJson([
                     'data' => $newInvoice->attributesToArray()
                 ]);
        $this->assertDatabaseHas('invoices', $newInvoice->attributesToArray());
        $this->assertDatabaseMissing('invoices', $invoice->attributesToArray());
    }

    public function test_user_can_delete_invoice()
    {
        $invoice = factory(Invoice::class)->state('withCustomer')->create();

        $response = $this->actingAs($invoice->customer->user, 'api')->deleteJson(route('invoices.destroy', $invoice->id));

        $response->assertStatus(204);

        $this->assertDatabaseMissing('invoices', $invoice->attributesToArray());
    }

    public function test_user_can_list_invoices()
    {
        $customer = factory(Customer::class)->state('withUser')->create();
        $invoices = factory(Invoice::class, 10)->raw();

        $customer->invoices()->createMany($invoices);

        $response = $this->actingAs($customer->user, 'api')->getJson(route('invoices.index'));

        $response->assertStatus(200)
                 ->assertJson([
                     'data' => usort($invoices, function ($a, $b) {
                         return $a['published_at'] <=> $b['published_at'];
                     })
                 ]);
    }
}
