<?php

namespace Tests\Feature;

use App\User;
use App\Invoice;
use App\Customer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomersTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_store_customer()
    {
        $user = factory(User::class)->create();

        $customer = factory(Customer::class)->raw();

        $response = $this->actingAs($user, 'api')->postJson(route('customers.store'), $customer);

        $response->assertStatus(201);

        $this->assertDatabaseHas('customers', $customer);
    }

    public function test_user_needs_to_be_logged_in_to_store()
    {
        $customer = factory(Customer::class)->raw();
        $response = $this->postJson(route('customers.store'), $customer);

        $response->assertStatus(401);

        $this->assertDatabaseMissing('customers', $customer);
    }

    public function test_user_can_update_customer()
    {
        $customer = factory(Customer::class)->state('withUser')->create();
        $newCustomer = factory(Customer::class)->raw();

        $response = $this->actingAs($customer->user, 'api')->putJson(route('customers.update', $customer->id), $newCustomer);

        $response->assertStatus(200)
                 ->assertJson([
                     'data' => $newCustomer
                 ]);

        $this->assertDatabaseHas('customers', $newCustomer);
        $this->assertDatabaseMissing('customers', $customer->attributesToArray());
    }

    public function test_user_can_delete_customer()
    {
        $customer = factory(Customer::class)->state('withUser')->create();

        $response = $this->actingAs($customer->user, 'api')->deleteJson(route('customers.destroy', $customer->id));

        $response->assertStatus(204);

        $this->assertDatabaseMissing('customers', $customer->attributesToArray());
    }

    public function test_user_can_show_customer()
    {
        $customer = factory(Customer::class)->state('withUser')->create();

        $response = $this->actingAs($customer->user, 'api')->getJson(route('customers.show', $customer->id));

        $response->assertStatus(200)
                 ->assertJsonCount(14, 'data')
                 ->assertJson([
                     'data' => [
                         'name' => $customer->name,
                         'email' => $customer->email,
                         'phone' => $customer->phone,
                         'ico' => $customer->ico,
                         'dic' => $customer->dic,
                         'city' => $customer->city,
                         'street' => $customer->street,
                         'house_number' => $customer->house_number,
                         'post_code' => $customer->post_code
                     ]
                 ]);
    }

    public function test_user_can_index_customers()
    {
        $user = factory(User::class)->create();
        $customers = factory(Customer::class, 10)->raw();
        $user->customers()->createMany($customers);

        $response = $this->actingAs($user, 'api')->getJson(route('customers.index'));

        $response->assertStatus(200)
                 ->assertJson([
                     'data' => usort($customers, function ($a, $b) {
                         return $a['name'] <=> $b['name'];
                     })
                 ]);
    }

    public function test_user_cant_update_others_customers()
    {
        $customer = factory(Customer::class)->state('withUser')->create();
        $userIlegal = factory(User::class)->create();

        $newCustomer = factory(Customer::class)->raw();

        $response = $this->actingAs($userIlegal, 'api')->putJson(route('customers.update', $customer->id), $newCustomer);

        $response->assertStatus(403);

        $this->assertDatabaseHas('customers', $customer->attributesToArray());
        $this->assertDatabaseMissing('customers', $newCustomer);
    }

    public function test_customers_store_validation_errors()
    {
        $user = factory(User::class)->create();
        
        $response = $this->actingAs($user, 'api')->postJson(
            route('customers.store'),
            [
                'email' => 'badEmail@test.'
            ]
        );

        $response->assertStatus(422)
                 ->assertJsonValidationErrors(['email', 'name']);

        $this->assertDatabaseMissing('customers', ['email' => 'badEmail@test.']);
    }

    public function test_customers_update_validation_errors()
    {
        $customer = factory(Customer::class)->state('withUser')->create();
        
        $response = $this->actingAs($customer->user, 'api')->putJson(
            route('customers.update', $customer->id),
            [
                'email' => 'badEmail@test.'
            ]
        );

        $response->assertStatus(422)
                 ->assertJsonValidationErrors(['email']);

        $this->assertDatabaseMissing('customers', ['email' => 'badEmail@test.']);
    }

    public function test_customers_invoices_counts_are_right()
    {
        $customer = factory(Customer::class)->state('withUser')->create()->setHidden(['created_at', 'updated_at']);

        $payedCount = 15;
        $customer->invoices()->createMany(factory(Invoice::class, $payedCount)->state('payed')->raw());

        $unpayedCount = 10;
        $customer->invoices()->createMany(factory(Invoice::class, $unpayedCount)->state('unpayed')->raw());

        $expiredCount = 7;
        $customer->invoices()->createMany(factory(Invoice::class, $expiredCount)->state('expired')->raw());

        $response = $this->actingAs($customer->user, 'api')->getJson(route('customers.show', $customer->id));

        $response->assertStatus(200)
                 ->assertJson([
                     'data' => [
                         'all_invoices_count' => $payedCount + $unpayedCount + $expiredCount,
                         'unpayed_invoices_count' => $unpayedCount,
                         'expired_invoices_count' => $expiredCount
                     ]
                 ]);
    }
}
