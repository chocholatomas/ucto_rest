<?php

namespace Tests\Feature;

use App\User;
use App\Invoice;
use App\Customer;
use App\InvoiceItem;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InvoiceItemsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_store_invoice_item()
    {
        $invoice = factory(Invoice::class)->state('withCustomer')->create();
        $item = factory(InvoiceItem::class)->make()->setHidden(['created_at', 'updated_at']);

        $response = $this->actingAs($invoice->customer->user, 'api')->postJson(route('invoices.invoiceItems.store', $invoice->id), $item->attributesToArray());

        $response->assertStatus(201)
                 ->assertJson([
                     'data' => $item->attributesToArray()
                 ]);

        $this->assertDatabaseHas('invoice_items', $item->attributesToArray());
    }

    public function test_user_can_delete_invoice_item()
    {
        $item = factory(InvoiceItem::class)->state('withInvoice')->create()->setHidden(['created_at', 'updated_at']);
        
        $response = $this->actingAs($item->invoice->customer->user, 'api')->deleteJson(route('invoiceItems.destroy', $item->id));

        $response->assertStatus(204);

        $this->assertDatabaseMissing('invoice_items', $item->attributesToArray());
    }

    public function test_user_can_update_invoice_item()
    {
        $item = factory(InvoiceItem::class)->state('withInvoice')->create()->setHidden(['created_at', 'updated_at']);
        $newItem = factory(InvoiceItem::class)->make()->setHidden(['created_at', 'updated_at']);

        $response = $this->actingAs($item->invoice->customer->user, 'api')->putJson(route('invoiceItems.update', $item->id), $newItem->attributesToArray());

        $response->assertStatus(200)
                 ->assertJson([
                     'data' => $newItem->attributesToArray()
                 ]);

        $this->assertDatabaseHas('invoice_items', $newItem->attributesToArray());
        $this->assertDatabaseMissing('invoice_items', $item->attributesToArray());
    }
}
