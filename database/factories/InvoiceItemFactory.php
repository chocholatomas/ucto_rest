<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Invoice;
use App\InvoiceItem;
use Faker\Generator as Faker;

$factory->define(InvoiceItem::class, function (Faker $faker) {
    return [
        'name' => $faker->words(5, true),
        'count' => $faker->randomDigitNotNull,
        'unit' => 'ks',
        'price_per_unit' => $faker->randomFloat(2, 100, 20000)
    ];
});

$factory->state(InvoiceItem::class, 'withInvoice', function (Faker $faker) {
    return [
        'invoice_id' => factory(Invoice::class)->state('withCustomer')->create()->id
    ];
});
