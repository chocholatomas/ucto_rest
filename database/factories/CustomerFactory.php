<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\User;
use App\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'city' => $faker->city,
        'street' => $faker->optional()->streetName,
        'house_number' => $faker->buildingNumber,
        'post_code' => $faker->postCode,
        'email' => $faker->optional()->email,
        'phone' => $faker->optional()->phoneNumber,
        'ico' => $faker->optional()->ico,
        'dic' => function ($customer) use ($faker) {
            return empty($customer['ico']) ? null : $faker->optional()->ico;
        }
    ];
});

$factory->state(Customer::class, 'withUser', function () {
    return [
        'user_id' => factory(User::class)->create()->id
    ];
});