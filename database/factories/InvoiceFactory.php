<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Invoice;
use App\Customer;
use Faker\Generator as Faker;

$factory->define(Invoice::class, function (Faker $faker) {
    return [
        'name' => $faker->optional()->words(5, true),
        'number' => $faker->unique()->randomNumber(3),
        'published_at' => $faker->dateTimeBetween('-2 years', '-1 year'),
        'expires_at' => $faker->dateTimeBetween('-1 year', '+3 months'),
        'payed_at' => $faker->optional()->dateTimeBetween('-1 year'),
        'payment_type' => $faker->randomElement(['hotově', 'bankovním převodem'])
    ];
});

$factory->state(Invoice::class, 'withCustomer', function () {
    return [
        'customer_id' => factory(Customer::class)->state('withUser')->create()->id
    ];
});

$factory->state(Invoice::class, 'payed', function (Faker $faker) {
    return [
        'payed_at' => $faker->dateTimeThisYear()
    ];
});

$factory->state(Invoice::class, 'unpayed', function (Faker $faker) {
    return [
        'expires_at' => $faker->dateTimeBetween('+1 week', '+3 months'),
        'payed_at' => null
    ];
});

$factory->state(Invoice::class, 'expired', function (Faker $faker) {
    return [
        'expires_at' => $faker->dateTimeThisYear(),
        'payed_at' => null
    ];
});