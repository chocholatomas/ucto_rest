<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::apiResource('customers', 'CustomerController');

    Route::apiResource('customers.invoices', 'InvoiceController')->only('store');
    Route::apiResource('invoices', 'InvoiceController')->except('store');

    Route::apiResource('invoices.invoiceItems', 'InvoiceItemController')->only('store');
    Route::apiResource('invoiceItems', 'InvoiceItemController')->only('update', 'destroy');
});
