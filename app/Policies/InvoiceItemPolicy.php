<?php

namespace App\Policies;

use App\User;
use App\InvoiceItem;
use Illuminate\Auth\Access\HandlesAuthorization;

class InvoiceItemPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the invoice item.
     *
     * @param  \App\User  $user
     * @param  \App\InvoiceItem  $invoiceItem
     * @return mixed
     */
    public function update(User $user, InvoiceItem $invoiceItem)
    {
        return $user->id == $invoiceItem->invoice->customer->user_id;
    }
}
