<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\InvoiceItem;
use Illuminate\Http\Request;
use App\Http\Resources\InvoiceItemResource;

class InvoiceItemController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $invoice)
    {
        $invoice = auth()->user()->invoices()->findOrFail($invoice);

        return new InvoiceItemResource(
            $invoice->items()->create($this->validateRequest($request))
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvoiceItem  $invoiceItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvoiceItem $invoiceItem)
    {
        $this->authorize('update', $invoiceItem);

        $invoiceItem->update($this->validateRequest($request));

        return new InvoiceItemResource($invoiceItem);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvoiceItem  $invoiceItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoiceItem $invoiceItem)
    {
        $this->authorize('update', $invoiceItem);

        $invoiceItem->delete();

        return response(null, 204);
    }

    public function validateRequest($request)
    {
        return $request->validate([
            'name' => 'sometimes|required|string',
            'count' => 'sometimes|required|integer|min:1',
            'unit' => 'sometimes|required|string',
            'price_per_unit' => 'sometimes|required|numeric|min:0'
        ]);
    }
}
