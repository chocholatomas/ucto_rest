<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Resources\CustomerResource;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CustomerResource::collection(
            auth()->user()->customers()->orderBy('name')->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = auth()->user()->customers()->create($this->validatePostRequest($request));

        return new CustomerResource($customer);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        $this->authorize('update', $customer);

        return new CustomerResource($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $this->authorize('update', $customer);

        $customer->update($this->validateUpdateRequest($request));

        return new CustomerResource($customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $this->authorize('update', $customer);

        $customer->delete();

        return response(null, 204);
    }

    public function validatePostRequest($request) {
        return $request->validate([
            'name' => 'required|string',
            'city' => 'required|string',
            'street' => 'nullable|string',
            'house_number' => 'required|string',
            'post_code' => 'required|string',
            'email' => 'nullable|string|email',
            'phone' => 'nullable|string',
            'ico' => 'nullable|string',
            'dic' => 'nullable|string'
        ]);
    }

    public function validateUpdateRequest($request) {
        return $request->validate([
            'name' => 'sometimes|required|string',
            'city' => 'sometimes|required|string',
            'street' => 'sometimes|nullable|string',
            'house_number' => 'sometimes|required|string',
            'post_code' => 'sometimes|required|string',
            'email' => 'sometimes|nullable|string|email',
            'phone' => 'sometimes|nullable|string',
            'ico' => 'sometimes|nullable|string',
            'dic' => 'sometimes|nullable|string'
        ]);
    }
}
