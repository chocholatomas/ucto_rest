<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Http\Request;
use App\Http\Resources\InvoiceResource;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return InvoiceResource::collection(
            auth()->user()->invoices()->orderBy('published_at', 'desc')->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $customer)
    {
        $customer = auth()->user()->customers()->findOrFail($customer);

        return new InvoiceResource(
            $customer->invoices()->create($this->validateStoreRequest($request))
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        $this->authorize('update', $invoice);

        return new InvoiceResource($invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        $this->authorize('update', $invoice);

        $invoice->update($this->validateUpdateRequest($request));

        return new InvoiceResource($invoice);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        $this->authorize('update', $invoice);

        $invoice->delete();

        return response(null, 204);
    }

    public function validateStoreRequest($request) {
        return $request->validate([
            'name' => 'nullable|string',
            'number' => 'nullable|integer|min:1',
            'published_at' => 'required|date',
            'expires_at' => 'required|date|after:published_at',
            'payed_at' => 'nullable|date|after:published_at',
            'payment_type' => 'required|string'
        ]);
    }

    public function validateUpdateRequest($request) {
        return $request->validate([
            'name' => 'sometimes|nullable|string',
            'number' => 'sometimes|nullable|integer|min:1',
            'published_at' => 'sometimes|required|date',
            'expires_at' => 'sometimes|required|date|after:published_at',
            'payed_at' => 'sometimes|nullable|date|after:published_at',
            'payment_type' => 'sometimes|required|string'
        ]);
    }
}
