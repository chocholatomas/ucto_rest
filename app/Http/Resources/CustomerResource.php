<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,

            'name' => $this->name,

            'city' => $this->city,
            'street' => $this->street,
            'house_number' => $this->house_number,
            'post_code' => $this->post_code,

            'email' => $this->email,
            'phone' => $this->phone,
            'ico' => $this->ico,
            'dic' => $this->dic,

            'all_invoices_count' => $this->invoices()->count(),
            'unpayed_invoices_count' => $this->invoices()->unpayed()->count(),
            'expired_invoices_count' => $this->invoices()->expired()->count()
        ];
    }
}
