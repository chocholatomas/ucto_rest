<?php

namespace App\Http\Resources;

use App\Http\Resources\InvoiceItemResource;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            
            'name' => $this->name,
            'number' => $this->number,
            'published_at' => $this->published_at,
            'expires_at' => $this->expires_at,
            'payed_at' => $this->payed_at,
            'payment_type' => $this->payment_type,

            'items' => InvoiceItemResource::collection($this->items)
        ];
    }
}
