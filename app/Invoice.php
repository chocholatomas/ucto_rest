<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'name',
        'number',
        'published_at',
        'expires_at',
        'payed_at',
        'payment_type'
    ];

    protected $dates = [
        'published_at',
        'expires_at',
        'payed_at'
    ];

    protected $casts = [
        'number' => 'integer',
        'published_at' => 'timestamp:U',
        'expires_at' => 'timestamp:U',
        'payed_at' => 'timestamp:U'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function items()
    {
        return $this->hasMany(InvoiceItem::class);
    }

    public function scopePayed($query)
    {
        return $query->whereNotNull('payed_at');
    }

    public function scopeUnpayed($query)
    {
        return $query->whereNull('payed_at')->whereDate('expires_at', '>=', now());
    }

    public function scopeExpired($query)
    {
        return $query->whereNull('payed_at')->whereDate('expires_at', '<', now());
    }

    public function getSumAttribute()
    {
        return $this->items->sum('sum');
    }
}
