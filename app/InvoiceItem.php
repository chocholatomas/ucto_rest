<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    protected $fillable = [
        'name',
        'count',
        'unit',
        'price_per_unit'
    ];

    protected $casts = [
        'count' => 'integer',
        'price_per_unit' => 'decimal:2'
    ];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function getSumAttribute()
    {
        return number_format($this->count * $this->price_per_unit, 2, '.', '');
    }
}
